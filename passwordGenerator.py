#!/usr/bin/python3

import secrets, string, pyperclip

# https://docs.python.org/3/library/secrets.html

# Even if `"` looks like a double `'`, let's keep `"` as we can most of time check which characters they are, thanks to the password length.
symbols = '!"#$%&\'()*+,-./:;<=>?@[\]^_`{}~'
symbolsNotAmbiguous = '!"#$%&\'()*+,-./:;<=>?@[\]^_`{}~'

numbers = string.digits
numbersNotAmbiguous = '23456789'

lowercaseLetters = string.ascii_lowercase
lowercaseLettersNotAmbiguous = 'abcdefghjkmnpqrstuvwxyz'
uppercaseLetters = string.ascii_uppercase
uppercaselettersNotAmbiguous = 'ABCDEFGHJKLMNPQRSTUVWXYZ'

charset = numbersNotAmbiguous + lowercaseLettersNotAmbiguous + uppercaselettersNotAmbiguous + symbolsNotAmbiguous
passwordLen = 20

charsetLen = len(charset)

"""
To comply with password requirements, currently enforce to:
- have at least:
* at least 1 lowercase and uppercase characters
* 1 digit
* 1 a symbol
- be of maximum 18 characters
"""

##

while True:
    password = ''
    for passwordIndex in range(passwordLen):
        charsetIndex = secrets.randbelow(charsetLen)
        charsetEl = charset[charsetIndex]
        password += charsetEl
    if any(char.isdigit() for char in password) and any(char.islower() for char in password) and any(char.isupper() for char in password):
        break

pyperclip.copy(password)
print(password)
